﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class screenElementGenerator : MonoBehaviour
{
    public GameObject screenElement;//環狀螢幕粒子

    public float screenElementThetaX = 1.0f;//環狀螢幕粒子角度X
    public float screenElementThetaY = 1.0f;//環狀螢幕粒子角度Y

    public float screenRadius = 27f;//環狀螢幕半徑
    public float screenInitHeight = 0f;//環狀螢幕初始高度
    public float screenHeight = 15f;//環狀螢幕最終高度
    public GameObject[,] screenElementPool = new GameObject[16, 240];

    public GameObject pool;
    public Color initColor, toColor;

    //public GameObject screenElementPrefab;

    private MaterialPropertyBlock _propBlock;
    //private Renderer _renderer;

    private void Awake()
    {
        _propBlock = new MaterialPropertyBlock();
        //_renderer = screenElementPrefab.gameObject.GetComponent<Renderer>();
    }

    void Start()
    { 
        int count = 0;
        int x, y;
        x = -1;
        for (float height = screenHeight; height > 0.0f; height-=screenElementThetaY)
        {
            x++;
            y = -1;
            for (float theta = 0; theta < 360; theta += screenElementThetaX)
            {
                y++;
                float posX = (screenRadius * Mathf.Cos(90 - (theta * Mathf.PI / 180)));
                float posZ = (screenRadius * Mathf.Sin(90 - (theta * Mathf.PI / 180)));
                float posY = height;
                GameObject tempScreenElement = Instantiate(screenElement, new Vector3(posX, posY, posZ), new Quaternion(0, 0, 0, 0));
                screenElementPool[x, y] = tempScreenElement;
                tempScreenElement.transform.parent = pool.transform;
                count++;
            }
        }
        print(count);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ClearGraphPixle();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            DrawGraphPixleRandom();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            DrawGraphSin();
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            DrawArray(GraphTested.graphArrayOne);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            DrawArray(GraphTested.graphArrayDel);
        }
    }
    void ClearGraphPixle()
    {
        for(int i = 0; i < 16; i++)
        {
            for(int j = 0; j < 240; j++)
            {
                Renderer _renderer = screenElementPool[i, j].GetComponent<Renderer>();
                _renderer.GetPropertyBlock(_propBlock);
                _propBlock.SetColor("_Color", initColor);
                _renderer.SetPropertyBlock(_propBlock);
            }
        }
    }
    void DrawGraphPixleRandom()
    {
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 240; j++)
            {
                Color randomColor;
                //randomColor = new Color(Random.Range(0f, 255f), Random.Range(0f, 255f), Random.Range(0f, 255f), 255);
                randomColor = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f);
                Renderer _renderer = screenElementPool[i, j].GetComponent<Renderer>();
                _renderer.GetPropertyBlock(_propBlock);
                _propBlock.SetColor("_Color", Color.Lerp(initColor, randomColor, 1.5f));
                _renderer.SetPropertyBlock(_propBlock);
            }
        }
    }
    void DrawGraphSin()
    {
        int x = 0;
        int y;
        while(x < 100)
        {
            y = Mathf.RoundToInt((7 * Mathf.Sin(x / 2) + 7));
            Color randomColor;
            randomColor = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f);
            Renderer _renderer = screenElementPool[y, x].GetComponent<Renderer>();
            _renderer.GetPropertyBlock(_propBlock);
            _propBlock.SetColor("_Color", Color.Lerp(initColor, randomColor, 1.5f));
            _renderer.SetPropertyBlock(_propBlock);
            x++;
            Debug.Log(new Vector2(y, x));
        }
    }
    void DrawArray(string[] graph)
    {
        foreach (string i in graph)
        {
            string tX = "", tY = "";
            bool preX = false, preY = false;
            foreach (char j in i)
            {
                if (j == '}') continue;
                if (j == '{')
                {
                    preX = true;
                    continue;
                }
                if (j == ',')
                {
                    preX = false;
                    preY = true;
                    continue;
                }
                if (preX)
                {
                    tX += j;
                    continue;
                }
                if (preY)
                {
                    tY += j;
                    continue;
                }
            }
            //Debug.Log(new Vector2(System.Int32.Parse(tX), System.Int32.Parse(tY)
            //Debug.Log(tX);
            //Debug.Log(tY);
            Color randomColor;
            randomColor = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f);
            Renderer _renderer = screenElementPool[short.Parse(tX), short.Parse(tY)].GetComponent<Renderer>();
            _renderer.GetPropertyBlock(_propBlock);
            _propBlock.SetColor("_Color", Color.Lerp(initColor, toColor, 1.5f));
            _renderer.SetPropertyBlock(_propBlock);
        }
    }

}
