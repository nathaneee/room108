﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveController : MonoBehaviour
{
    private CharacterController viewer;
    private float playerSpeed = 5.0f;

    public GameObject camaeraHandler;

    public float mouseXSensitivity;
    public float mouseYSensitivity;

    Vector3 cameraFollowVel;

    void Start()
    {
        viewer = gameObject.AddComponent<CharacterController>();
    }

    void Update()
    {
        playerSpeed = (Input.GetKey(KeyCode.LeftShift)) ? 10.0f : 5.0f;

        if (Input.GetKey(KeyCode.W))
            viewer.SimpleMove(transform.forward * playerSpeed);
        if (Input.GetKey(KeyCode.S))
            viewer.SimpleMove(transform.forward * -playerSpeed);
        if (Input.GetKey(KeyCode.A))
            viewer.SimpleMove(transform.right * -playerSpeed);
        if (Input.GetKey(KeyCode.D))
            viewer.SimpleMove(transform.right * playerSpeed);
    }
    private void FixedUpdate()
    {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            mouseX = Mathf.Clamp(mouseX, -1f, 1f);
            mouseY = Mathf.Clamp(mouseY, -1f, 1f);
            Vector2 mouseInputXY = new Vector2(mouseX, mouseY);
            float cameraSmoothTime = 5f;

            Vector3 cameraRotate = Vector3.up * mouseInputXY.x * mouseXSensitivity + Vector3.right * -mouseInputXY.y * mouseYSensitivity;
            Vector3 cameraTarget = Vector3.SmoothDamp(camaeraHandler.transform.localEulerAngles, camaeraHandler.transform.localEulerAngles + cameraRotate, ref cameraFollowVel, cameraSmoothTime / 100);

            camaeraHandler.transform.localRotation = Quaternion.Euler(cameraTarget.x, 0, 0);
            this.transform.localRotation = Quaternion.Euler(0, this.transform.localEulerAngles.y + cameraRotate.y, 0);
        }
}
